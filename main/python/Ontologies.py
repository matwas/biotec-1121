# class for single instance of term
import re
import csv
import copy


class Term:
    def __init__(self, term_id, term_type, name=None, definition=None, synonyms_passed=None, is_a=None):
        if name != None:
            name = self.__conditional_lowercase(name)

        self.attributes = {'id' : term_id,
                           'type' : term_type,
                           'name' : name,
                           'synonym' : synonyms_passed,
                           'is_a' : is_a}

        self.rank = 0
        self.rank_map = dict()


        if self.attributes['synonym'] is None:
            self.attributes['synonym'] = set()
            # self.synonyms = set()

        if is_a is None:
            self.attributes['is_a'] = set()
            # self.is_a = set()
        if definition is not None:
            self.attributes['def'] = definition

        # self.other = dict()
        # additional class attributes should be added for custom relations.
        # if many different are expected, a dictionary could be a reasonable solution.

    def __str__(self):
        # check if this term is printable
        self.__printable()

        text = '[Term]' + '\n'
        try:
            for key, value in self.attributes.items():
                if type(value) == set:
                    for element in value:
                        element = self.__remove_comments(element)
                        text += str(key) + ': ' + str(element) + '\n'
                else:
                    text += key + ': ' + value + '\n'
            text += '\n'
        except:
            print(key, value)
            print(self.attributes)
            raise Exception()
        return text

    def __conditional_lowercase(self, string):
        parts = string.split()
        result_parts = []
        for part in parts:
            if len(part) > 3:
                result_parts.append(part.lower())
            else:
                result_parts.append(part)
        return ' '.join(result_parts)

    def __printable(self):
        if self.attributes['id'] == None:
            raise Exception('This term does not have an ID!')
        if self.attributes['type'] == None:
            raise Exception('Term with ID:' + self.attributes['id'] + 'does not have a type!')
        if self.attributes['name'] == None:
            raise Exception('Term with ID:' + self.attributes['id'] + 'does not have a name!')
        return True

    # lowercase text, strip trailing dots, commas and whitespaces
    def __preprocess(self, name):
        name = self.__conditional_lowercase(name)
        name = name.strip(' .,')
        return name

    def __remove_comments(self, string, separator='!'):
        actual_separator = ' ' + separator + ' '

        try:
            string_list = string.split(actual_separator, 1)
        except:
            return string
        return string_list[0]

    def verify_synonyms(self):
        new_synonyms = set()
        check=False
        for synonym in self.attributes['synonym']:
            if synonym.rstrip('\n') != '':
                new_synonyms.add(synonym)
            # else:
                # print("empty: ", synonym)
                # check=True
        self.attributes['synonym'] = new_synonyms
        # if check:
        #    print(self.attributes['synonym'])

    # delete all attributes which are not in the passed iterable
    def drop(self, to_leave):
        to_delete = set()
        for key in self.attributes.keys():
            if key not in to_leave:
                to_delete.add(key)
        for key in to_delete:
            del self.attributes[key]

    # remove EXACT[], "", and like from synonyms of the term.
    def reformat_synonyms(self):
        reformat_regex = re.compile('(^\"([^\"]*)\")|(^[^\"].*)')
        new_synonyms = set()

        self.verify_synonyms()
        synonyms = self.attributes['synonym']

        for synonym in synonyms:
            # print(synonyms)
            try:
                matches = reformat_regex.search(synonym).groups()
            except:
                # print(self.__str__())
                # print(synonyms)
                # print(synonym)
                raise Exception()

            result = matches[2]
            if result == None:
                result = matches[1]
            new_synonyms.add(result)
        self.attributes['synonym'] = new_synonyms

    # add name to the ontology; if rank is not greater than the current one, the new name will be added as a synonym
    def add_name(self, name, rank=None, preprocess=True):
        """Synonyms are by definition equivalent to name, but for convenience and possibly to increase efficiency
        of galactic we want the most accurate of them to be the name. If no rank is provided, the first name to
        be passed will be assigned to self.name. However, if there are ranks, they will be (a) mapped to their
        numeric value using rank_map={}, (b) the synonym with highest rank will be chosen as the name.
        """

        # if name is empty don't add it
        if len(name) == 0 or name == None:
            return False

        # preprocess the name
        if preprocess:
            name = self.__preprocess(name)

        # if synonym is the same as name, don't add it
        if name == self.attributes['name']:
            return False

        # substitute rank with its mapped value
        if rank in self.rank_map.keys():
            rank = self.rank_map[rank]
        else:
            rank = 0

        # if new acronym is better, replace the old one and the name
        if rank > self.rank:
            # add current name to the synonyms
            if self.attributes['name'] != None:
                self.attributes['synonym'].add(self.attributes['name'])
            self.attributes['name'] = name
            self.rank = rank
            return True

        # else, add as a synonym
        if self.attributes['name'] == name:
            return False
        else:
            self.attributes['synonym'].add(name)
            return False

    def add_parent(self, parent_id):
        self.attributes['is_a'].add(parent_id)

    def names_and_synonyms(self):
        names_synonyms = set()
        names_synonyms.add(self.attributes['name'])
        for synonym in self.attributes['synonym']:
            names_synonyms.add(synonym)
        return names_synonyms

    # checks for equivalence of two terms
    def equivalent(self, term):
        try:
            for key, value in self.attributes.items():
                if term.attributes[key] != value:
                    return False
        except:
            return False
        return True

class Term_Map:
    def __init__(self):
        self.terms = dict()
        self.ids = set()
        self.relevant_attributes = {'id', 'name', 'type', 'is_a', 'synonym'}



    # add term to Term_map
    # if conflict, addition is rejected
    def add_term(self, term):
        if term.attributes['id'] in self.ids:
            return False
        else:
            self.ids.add(term.attributes['id'])
            self.terms[term.attributes['id']] = term
            return True


    # add whole set of terms to self
    def initialize_terms(self, ids_set):
        for element in ids_set:
            self.terms[element] = Term(element)
        self.ids.update(ids_set)


    def get_ids(self):
        return self.ids


    def get_names_and_synonyms(self):
        names_synonyms = set()
        for term in self.terms.values():
            term_ns = term.names_and_synonyms()
            names_synonyms.union(term_ns)
        return names_synonyms


    def get_terms_id_prefix(self, list_types):
        custom_map = Term_Map()
        for given_type in list_types:
            for value in self.terms.values():
                id_parts = value.attributes['id'].split(':')
                if id_parts[0] == given_type:
                    custom_map.add_term(value)
        return custom_map


    def remove_term(self, term_id):
        del self.terms[term_id]


    def get_term(self, term_id):
        return self.ids(term_id)


    def print_to_file(self, path):
        with open(path, mode='w') as output:
            for term in self.terms.values():
                output.write(term.__str__())


    # _reads particular term
    # should be initialized just after file has read '[Term]', and read everything until an empty line or another term.
    def read_term(self, file, save=True, drop=True, write=False, output_file=None, reformat=True):
        # initialize a new term
        new_term = Term(None, None)
        new_term.rank = 10

        # loop until broken
        while True:
            # read a line
            line = file.readline()
            if line:
                line = line.rstrip('\n')
                # if line is empty
                if line == '' or line ==' ':
                    # break the while loop
                    break
                # if the line is [Term], two separate records are not separated by a newline
                elif line == '[Term]':
                    print('Wrong formatting within the ontology')
                    # this can be handled by this method, but should be avoided for clarity of the ontology
                    output_file.write('\n')
                    self.read_term(file, save=save, drop=drop, write=write, reformat=reformat, output_file=output_file)
                    # self.read_term(file)
                    break
                # else, the line has some content and is not term - hence it must be a line belonging to that record
                else:
                    # split the line over the first colon
                    try:
                        key, value = line.split(':', 1)
                        key = key.rstrip()
                        value = value.strip()
                    except:
                        print("The line is: \n")
                        print(repr(line))
                        print("Current term is:")
                        print(new_term.__str__())
                        raise Exception('Line encountered does not have a colon - it probably is not a part of!')

                    if key not in new_term.attributes.keys():
                        new_term.attributes[key] = set()

                    if type(new_term.attributes[key]) == set:
                        # print("Setting")
                        new_term.attributes[key].add(value)
                    else:
                        new_term.attributes[key] = value
            else:
                break

        if drop:
            new_term.drop(self.relevant_attributes)
        if reformat:
            new_term.reformat_synonyms()
        if save:
            self.terms[new_term.attributes['id']] = new_term
            self.ids.add(new_term.attributes['id'])
        if write:
            output_file.write(new_term.__str__() + '\n')
        return new_term


    # reads whole ontology provided to it and adds it to the map
    # self, file, save=True, drop=True, write=False, output_file=None, reformat=True
    def read_ontology(self, path_existing_ontology, save=True, drop=True, write=False, reformat=True, output_file=None):
        # open ontology file
        with open(path_existing_ontology, mode='r') as f:
            added_terms = 0
            # loop until broken
            while True:

                # read a single line from the file
                line = f.readline()
                try:
                    # if line is not null
                    if line:
                        # strip newline
                        line = line.rstrip('\n ')
                        # if line contains [Term], hence starts record, delegate contol to _read_term method
                        if line == '[Term]':
                            term_now = self.read_term(f, save=save, drop=drop, write=write, reformat=reformat, output_file=output_file)
                            added_terms += 1
                    # if line is null (the file has ended) break the loop.
                    else:
                        break
                except:
                    print(term_now)
                    raise Exception("Unexpected error when reading ontology")
            # print how many terms were added
            print("Terms added: ", added_terms)

    def create_ontology_from_file(self, path, id_name):
        id_counter = 1
        with open(path, mode='r') as f:
            for line in f.readlines():
                term_id = id_name + f":{id_counter:06d}"
                new_term = Term(term_id, 'stopword', line)
                self.add_term(new_term)
                id_counter += 1

    # deletes terms of given type
    def drop_type(self, type_name):
        counter = 0
        to_delete = set()
        for term_id, term in self.terms.items():
            if term.attributes['type'] == type_name:
                counter += 1
                to_delete.add(term_id)
        for id_name in to_delete:
            del self.terms[id_name]
        print("Succesfully dropped: " + str(counter))
        return counter

    # Adds one ontology into the current one
    def add_ontology(self, ontology):
        succesful_counter = 0
        failure_counter = 0
        for id_name, term in ontology.terms.items():
            if self.add_term(term):
                succesful_counter += 1
            else:
                failure_counter += 1
        print("Succesfully added: " + str(succesful_counter) + '\nFailures: ' + str(failure_counter))

""""Supposed to be used to merge two ontologies into one. Produces a bunch of statistics.
Currently supporting only simple conflict resolution."""
class Ontology_Merger:
    def __init__(self, path_new, path_old):
        self.path_old = path_old
        self.path_new = path_new
        self.old_ontology = Term_Map()
        self.old_ontology.read_ontology(path_old)
        self.new_ontology = Term_Map()
        self.new_ontology.read_ontology(path_new)

    def types_statistics(self, ontology):
        types = dict()
        for term in ontology.terms.values():
            if term.attributes['type'] in types.keys():
                types[term.attributes['type']] += 1
            else:
                types[term.attributes['type']] = 1
        return types

    def merge(self):
        print("Merging: " + self.path_new + "\nInto: " + self.path_old)
        old_stats = self.types_statistics(self.old_ontology)
        new_stats = self.types_statistics(self.new_ontology)
        merged_ontology = copy.deepcopy(self.old_ontology)
        merged_ontology.add_ontology(self.new_ontology)
        same_counter = 0
        different_counter = 0
        new_counter = 0

        for id_name, term in merged_ontology.terms.items():
            if id_name not in self.old_ontology.terms.keys():
                new_counter += 1
            elif term.equivalent(self.old_ontology.terms[id_name]):
                same_counter += 1
            else:
                different_counter += 1
        print("Old ontology: " + str(old_stats))
        print("New ontology: " + str(new_stats))
        print("Terms without changes: " + str(same_counter))
        print("Terms with changes: " + str(different_counter))
        print("New terms: " + str(new_counter))

        return merged_ontology

