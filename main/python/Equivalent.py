from pyllist import dllist
import copy
import time



class AnnotationUnit:
    def __init__(self, tokenList, annotationSet, synonym):
        self.tokens = tokenList
        self.annotations = annotationSet
        self.synonym = synonym
        # self.subs = substitutable
        # self.synonyms = synonymList

    def __len__(self):
        return len(self.tokens)

    def __str__(self):
        return str(self.tokens) + ' : ' + str(self.annotations) + ' : ' + str(self.synonym)

    def __repr__(self):
        return str(self.tokens) + ' : ' + str(self.annotations) + ' : ' + str(self.synonym)

    def merge(self, annotation):
        if self.synonym != annotation.synonym:
            print(self.__str__())
            print(annotation)
            raise Exception("Different synonyms!")
        elif self.annotations != annotation.annotations:
            print(self.__str__())
            print(annotation)
            raise Exception("Different annotations!")

        for i in range(len(annotation.tokens)):
            self.tokens.appendleft(annotation.tokens.popright())
        self.annotations.update(annotation.annotations)

class Sentence:
    def __init__(self, sentence):
        self.original = sentence
        self.counter_exp = 1

        if sentence != None:
            self.check = True
            self.rawTokens = sentence[0]
            self.rawAnnotations = sentence[1]
            self.rawSynonyms = sentence[2]
            self.units, self.aID2aType, merge_set = self.__reformat(sentence)

            doomed = set()
            for merge_subset in merge_set:
                #                 print("IN: ")
                a = self.merge_units(merge_subset)
                #                 print("doomed: " + str(a))
                #                 print("type: " + str(type(a)))
                #                 print("len: " + str(len(a)))
                doomed.update(a)
            #             print("DOOMED!!!")
            #             print(doomed)

            self.units = [self.units[index] for index in range(len(self.units)) if index not in doomed]

        else:
            self.check = False
            self.rawTokens = None
            self.rawAnnotations = None
            self.rawSynonyms = None
            self.units = list()
            self.aID2aType = dict()

    def __len__(self):
        return len(self.aID2aType)

    def __str__(self):
        print("TYPE: " + str(type(self.units)))
        output = ""
        counter = 1
        for annotation in self.units:
            output += str(counter) + ": " + str(annotation) + '\n'
            counter += 1
        return output

    def merge_units(self, to_merge):
        main_index = max(to_merge) - 1
        doomed = set()
        for index in to_merge[:-1]:
            self.units[main_index].merge(self.units[index - 1])
            doomed.add(index - 1)
        return doomed

        # export to the format in which sentence was passed

    def export(self):
        tokens = list()
        annotations = dict()
        index = 1
        # THIS is the reason for inefficiency in the program!!!
        for unit in self.units:
            tokens += unit.tokens
            top_index = len(unit.tokens) + index

            for annotation in unit.annotations:
                try:
                    for i in range(index, top_index):
                        annotations[annotation].append(i)
                except:
                    annotations[annotation] = []
                    for i in range(index, top_index):
                        annotations[annotation].append(i)
            index = top_index

        # change aID into annotation names
        actual_annotations = dict()
        for key, value in annotations.items():
            try:
                actual_annotations[self.aID2aType[key]].append(value)
            except:
                actual_annotations[self.aID2aType[key]] = [value]
        self.counter_exp += 1
        return [tokens, actual_annotations]

    def __reformat(self, sentence):
        annotationsList = list()

        # map assigning each initial token position with a set of Annotation IDs
        pos2aID = {x: set() for x in range(1, len(self.rawTokens) + 1)}
        # map connecting Annotation IDs with types
        aID2aType = dict()

        # populating pos2aID map
        for aType, annotations in self.rawAnnotations.items():
            counter = 1
            for ann_seq in annotations:
                aID = aType + "-" + str(counter)
                aID2aType[aID] = aType
                for pos in ann_seq:
                    pos2aID[pos].add(aID)
                counter += 1

        # create
        pos2syn = dict()
        merge_set = set()

        for synID, tokNr in self.rawSynonyms.items():
            # print("In rawSynonyms loop")
            # print(self.rawSynonyms)
            termID = synID
            positions = tokNr
            # print("HERE: " + str(syn))
            for lst in positions:
                if len(lst) > 1:
                    # print(termID)
                    # print(sentence)
                    merge_set.add(tuple(lst))
                for pos in lst:
                    pos2syn[pos] = termID
            # print(pos2syn)

        # creating annotations objects from pos2aID and tokens
        for token_pos in range(1, len(self.rawTokens) + 1):
            thisToken = dllist()
            thisToken.append(self.rawTokens[token_pos - 1])
            # print("TOK: " + str(thisToken))
            theseAnnotations = pos2aID[token_pos]

            if token_pos in pos2syn.keys():
                thisSynonyn = pos2syn[token_pos]
            else:
                thisSynonyn = False

            newAnnotation = AnnotationUnit(thisToken, theseAnnotations, thisSynonyn)
            annotationsList.append(newAnnotation)

        return annotationsList, aID2aType, merge_set

class Generate:
    """
    A class containing methods generating synonyms from sentences. Parsing and id2synonyms mapping
    must be generated ourside.
    """
    def __init__(self, id2synonym):
        """

        :param id2synonym: dictionary containing mapping from term_id to the list of tokenized synonyms
        """
        self.id2synonym = id2synonym
        self.missing = dict()
        self.counter_global = 0

    def __generate_synonyms_list(self, unit):
        """
        Generate a list of synonyms for a particular unit.

        :param unit: AnnotationUnit
        :return: List of synonimical units
        """
        synonyms = self.id2synonym[unit.synonym]
        new_units = list()
        for syn in synonyms:
            new_units.append(AnnotationUnit(dllist(syn), unit.annotations, False))
        return new_units

    def generate_synonyms(self, sentence):
        """
        Method generating synonyms for a whole sentence.

        :param sentence with annotations and IDs, with overlapping IDs removed.
        :return: A list of synonymical sentences.
        """
        start_time = time.time()

        roots = [Sentence(None)]
        roots[0].aID2aType = sentence.aID2aType

        for unit in sentence.units:
            if unit.synonym == False:
                for root in roots:
                    root.units.append(unit)
            elif unit.synonym not in self.id2synonym.keys():
                try:
                    self.missing[unit.synonym] += 1
                except:
                    self.missing[unit.synonym] = 1
                finally:
                    for root in roots:          
                        root.units.append(unit)
            else:
                synonymic_units_map = {len(x) : x for x in self.__generate_synonyms_list(unit)}
                synonymic_units_map[len(unit)] = unit
                synonymic_units = synonymic_units_map.values()

                new_roots = list()

                for root in roots:
                    for synonymic_unit in synonymic_units:
                        new_root_synonym = copy.deepcopy(root)
                        new_root_synonym.units.append(synonymic_unit)
                        new_root_synonym.aID2aType = sentence.aID2aType
                        new_roots.append(new_root_synonym)
                roots = new_roots
        tm = time.time() - start_time

        self.counter_global += 1

        if tm > 2:
            print("time: " + str(tm))
        else:
            print(self.counter_global)

        return roots


