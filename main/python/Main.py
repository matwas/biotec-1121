#!/usr/bin/env python
# coding: utf-8

# In[1]:

import pickle
import json
import os
from collections import defaultdict
from collections import OrderedDict
import copy
# from pyllist import dllist
import numpy as np
import pandas as pd
from Equivalent import Sentence, AnnotationUnit, Generate
from IngestDoc import IngestDoc
from Ontologies import Term_Map, Term
import multiprocessing as mp
import time

class EquivalentGenerator:
    def __init__(self, min_len, max_len, path_ont, path_papers, path_map, path_pickle, path_export):
        self.ingest = IngestDoc()
        self.id2synonym = dict()

        # minimin lenght of sentence
        self.min_len = min_len
        # maximum lenght of sentence
        self.max_len = max_len
        self.size = self.max_len - self.min_len

        self.path_ont = path_ont
        self.path_papers = path_papers
        self.path_map = path_map
        self.path_pickle = path_pickle
        self.path_export = path_export

        # creating subdirectories for pickles
        if not os.path.exists(path_pickle + '/matrices'):
            os.mkdir(path_pickle + '/matrices')
        if not os.path.exists(path_pickle + '/sentences'):
            os.mkdir(path_pickle + '/sentences')

        # create ontology object and read it from file
        self.ontology = Term_Map()
        self.ontology.read_ontology(path_ont)
        self.loadID2SynonymMap()
        self.generate = Generate(self.id2synonym)

    def loadID2SynonymMap(self):
        # loading the id2synonym dictionary
        with open(self.path_map) as f:
            data = json.load(f)

        # creating id2synonym map (id to the list of tokenized synonyms)
        for term in data:
            self.id2synonym[term['id']] = term['synonyms']

        del data

    def getDoc(self, name):
        file = open('/home/mateuszw/Projects/BIOTEC-1121/resources/papers/' + name, 'r')
        doc = json.loads(file.read())
        return doc

    def processDocument(self, doc):
        sentences = self.ingest.get_sentences_with_annotations(doc)
        sentences_processed = [Sentence(self.ingest.remove_overlapping_annotations(sen)) for sen in sentences]
        sentences_processed_filtered = []
        for sentence in sentences_processed:
            if self.min_len <= len(sentence.units) < self.max_len:
                sentences_processed_filtered.append(sentence)
        print("senteces_processed_filtered: " + str(len(sentences_processed_filtered)))
        sentences_synonyms = [self.generate.generate_synonyms(sen) for sen in sentences_processed_filtered]

        return sentences_synonyms

    def export_batch(self, batch):
        sentences_final = [[syn.export() for syn in sentSyno] for sentSyno in batch]
        return sentences_final

    def getStatistics(self, synonymous_list):
        np_matrix = np.zeros((self.size, self.size), dtype=np.int)
        local_matrix = pd.DataFrame(np_matrix, columns=[str(x) for x in range(self.min_len, self.max_len)])
        local_matrix.index = local_matrix.index + self.min_len

        for sentence in synonymous_list:
            possible_len = set()
            for i in range(len(sentence)):
                if self.min_len <= len(sentence[i][0]) < self.max_len:
                    possible_len.add(len(sentence[i][0]))
            for x in possible_len:
                for y in possible_len:
                    local_matrix.at[x, str(y)] += 1
        return local_matrix

    def get_nk_sentences(self, processed_result, lenghts):
        """
        This method returns sentence pait which is then returned to json.
        :param processed_result:
        :param lenghts:
        :return:
        """

        result_sentence_tuples = []

        for sentenceList in processed_result:
            n_sentence = None
            k_sentence = None
            for sentence in sentenceList:
                if len(sentence[0]) == lenghts[0]:
                    n_sentence = sentence
                if len(sentence[0]) == lenghts[1]:
                    k_sentence = sentence
                if n_sentence != None and k_sentence != None:
                    result_sentence_tuples.append((n_sentence, k_sentence))
                    break
        print("Adding sentences: " + str(len(result_sentence_tuples)))
        return result_sentence_tuples


    def run(self, pickling=True, export=False, n=0, k=0):
        if export:
            output = open(self.path_export + '/sentences-' + str(max(n,k)) + '-' + str(min(n, k)) + '.json' , mode='w')
        matrix_pickle = self.path_pickle + '/matrices/'
        sentence_pickle = self.path_pickle + '/sentences/'

        np_matrix = np.zeros((self.size, self.size), dtype=np.int)
        matrix = pd.DataFrame(np_matrix, columns=[str(x) for x in range(self.min_len, self.max_len)])
        matrix.index = matrix.index + self.min_len

        files = sorted(os.listdir(self.path_papers), key=lambda filename: os.path.getsize(os.path.join(self.path_papers, filename)), reverse=True)

        sentence_len = {}
        added_counter = 0

        for f in files:
            print("FILE: " + f)
            try:
                # report error and proceed
                doc = self.getDoc(f)
                processed_sentences = self.processDocument(doc)

                previous_batch = 0
                count = 1
                for index in range(len(processed_sentences)):
                    for element in processed_sentences[index]:
                        if len(element.units) > 1000:
                            print("Too long - over 1000 units in a sentence!")
                            print(len(element.units))
                            print(element)
                        try:
                            sentence_len[len(element.units)] += 1
                        except:
                            sentence_len[len(element.units)] = 1
                        count += 1
                    if count > 50:
                        batch = processed_sentences[previous_batch:index]

                        count = 0
                        print("Batch size: " + str(len(batch)))
                        a = "Batch range: " + str(previous_batch) + ":" + str(index)
                        print(a)
                        if a == "Batch range: 354:366":
                            pass
                        previous_batch = index
                        processed = self.export_batch(batch)
                        count_processed = 0
                        for el in processed:
                            for e in el:
                                count_processed += 1
                        matrix = matrix.add(self.getStatistics(processed))
                        print("Matrix sum:" + str(matrix.sum(axis=0).sum()))

                        if pickling:
                            matrix.to_pickle('/home/mateuszw/Projects/BIOTEC-1121/resources/pickles/matrices/matrix-'
                                             + str(time.time()) + '.pkl')

                batch = processed_sentences[previous_batch:len(processed_sentences)]
                print("LAST BATCH!")
                print("Batch size: " + str(len(batch)))
                print("Batch range: " + str(previous_batch) + ":" + str(len(processed_sentences)))

                processed = self.export_batch(batch)
                processed_result = self.getStatistics(processed)

                if export and processed_result.at[n, str(k)] > 0:
                    sentences_batches = self.get_nk_sentences(processed, (n, k))
                    added_counter += len(sentences_batches)

                matrix = matrix.add(processed_result)
                print("Matrix sum: " + str(matrix.sum(axis=0).sum()))

                if pickling:
                    matrix.to_pickle(matrix_pickle + 'matrix-' + str(time.time()) + '.pkl')
            except:
                if pickling:
                    matrix.to_pickle(matrix_pickle + 'matrix-' + str(time.time()) + '.pkl')

                    # time.time()) + '.pkl', 'wb'
                    with open(sentence_pickle + 'sentence_lengths-' + str(time.time()) + '.pkl', 'wb') as handle:
                        pickle.dump(sentence_len, handle, protocol=pickle.HIGHEST_PROTOCOL)
                    raise
                else:
                    return matrix
            finally:
                print("In file: " + f + " added sentences: " + str(added_counter))
            if export:
                output.close()

        print("In total added sentences: " + str(added_counter))

        if pickling:
            print("Picking final!")
            matrix.to_pickle(matrix_pickle + 'matrix-' + str(time.time()) + '-FINAL.pkl')
        return matrix

def myrun():
    path_ont = '/home/mateuszw/Projects/BIOTEC-1121/resources/merged_ontologies.obo'
    path_papers = '/home/mateuszw/Projects/BIOTEC-1121/resources/papers'
    path_map = '/home/mateuszw/Projects/BIOTEC-1121/resources/synonyms_tokenized.json'
    path_pickle = '/home/mateuszw/Projects/BIOTEC-1121/resources/pickles'
    myrun = EquivalentGenerator(10, 60, path_ont, path_papers, path_map, path_pickle)
    myrun.run(True)