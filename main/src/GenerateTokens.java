import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import org.hamcrest.CoreMatchers;
import org.joda.time.chrono.StrictChronology;
import org.junit.Before;

import java.io.FileWriter;
import java.util.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

/**
 * GenerateTokens class tokenizes synonyms from ontologies. To do so, it:
 * (1) Parses json file with synonyms
 * (2) Creates a nlp pipeline and uses biorelate.tokenizer to tokenize the synonyms
 * (3) Retrieves the contents of tokens
 * (4) Generates a json file containing records with id and tokenized synonyms
 * Whole
 *
 * @author mateuszw
 * @version 1.0
 * @since 2019-09-03
 */
public class GenerateTokens {

    private StanfordCoreNLP pipeline;

    public static void main(String[] args) {
        run("/home/mateuszw/Projects/BIOTEC-1121/resources/synonyms_names.json",
                "/home/mateuszw/Projects/BIOTEC-1121/resources/synonyms_tokenized.json");
    }

    public static void run(String jsonIn, String jsonOut) {
        GenerateTokens nlp = new GenerateTokens();
        nlp.setup();
        // read in the json data
        JSONArray doc = readJson(jsonIn);
        // using this data, create a mapping from ids to synonyms.
        HashMap<String, List<String>> id2synonyms = nlp.getHashMap(doc);

        String id;
        List<List<String>> synonyms;
        JSONObject term;
        JSONArray id2synonymsTokens = new JSONArray();

        Iterator it = id2synonyms.entrySet().iterator();
        // iterate through
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            id = (String) pair.getKey();
            // actual tokenization happens here
            synonyms = nlp.tokenizeTerm((List<String>) pair.getValue());

            term = new JSONObject();
            // populate the JSONObject and add it to the hashMap
            term.put("id", id);
            term.put("synonyms", synonyms);
            id2synonymsTokens.add(term);

            it.remove(); // avoids a ConcurrentModificationException
        }
        // generate the json file
        nlp.writeJson(jsonOut, id2synonymsTokens);

    }

    private void setup() {
        Properties props = new Properties();
        props.setProperty("annotators", "biorelate.tokenize");
        props.setProperty("customAnnotatorClass.biorelate.tokenize",
                "com.biorelate.galactic.corenlpservice.components.BiorelateTokenizerAnnotator");

        this.pipeline = new StanfordCoreNLP(props);
    }

    private void writeJson(String path, JSONArray synonyms){
        //Write JSON file
        try (FileWriter file = new FileWriter(path)) {

            file.write(synonyms.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<List<String>> tokenizeTerm(List<String> synonyms){
        List<List<String>> synonymTokens = new ArrayList<>();

        for (String synonym : synonyms) {
            synonymTokens.add(this.tokenizeSingleSynonym(synonym));
        }
        return synonymTokens;
    }

    private static JSONArray readJson(String path) {
        //JSON parser object to parse read file
        //System.out.println(pipeline);

        JSONParser jsonParser = new JSONParser();
        JSONArray synonymsList = null;

        try (FileReader reader = new FileReader(path)) {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            synonymsList = (JSONArray) obj;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            return synonymsList;
        }

    }

    private HashMap<String, List<String>> getHashMap(JSONArray document){
        HashMap<String, List<String>> id2synonyms = new HashMap<>();

        for (int i = 0; i < document.size(); i++) {
            JSONObject term = (JSONObject) document.get(i);

            String id = (String) term.get("id");
            List<String> synonyms = (List<String>) term.get("synonyms");
            id2synonyms.put(id, synonyms);
        }
        return id2synonyms;
    }

    private List<String> tokenizeSingleSynonym(String synonym){
        Annotation sAnn = new Annotation(synonym + " ");
        pipeline.annotate(sAnn);

        List<CoreLabel> tokens = sAnn.get(CoreAnnotations.TokensAnnotation.class);
        List<String> stringTokens = new ArrayList<>();

        for(int i = 0; i < tokens.size() ; i++){
            stringTokens.add(tokens.get(i).toString());
        }
//        System.out.println(tokens.size());
//        System.out.println(tokens);

        return stringTokens;
    }
}