package com.biorelate.galactic.corenlpservice.components;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import com.biorelate.galactic.corenlpservice.components.CoreNLPAnnotator;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class BasicTest {

    @Before
    public void setup(){
        // creates a StanfordCoreNLP object, with POS tagging, lemmatization, NER, parsing, and coreference resolution
        Properties props = new Properties();
        props.setProperty("annotators", "biorelate.tokenize");
        props.setProperty("customAnnotatorClass.biorelate.tokenize",
                "com.biorelate.galactic.corenlpservice.components.BiorelateTokenizerAnnotator");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
    }

    @Test
    public void test1(){

        assertEquals(10, 10);
    }
}
